本次更新-V1.1.2
1.强化属性混淆适配swift项目；
2.强化类名混淆适配swift项目；
3.优化创建垃圾代码时,垃圾类可以设置为0；
4.修复属性混淆功能的部分xib混淆失败；
5.修复爬虫工具部分图片保存失败;
6.修复多次混淆错误太多（清除上个项目的遗留数据）;
7.修复垃圾类创建时类名相同bug；

教学视频:
链接:https://pan.baidu.com/s/1pqUkgU8YxSzjqTzpHlAD6g  密码:9sll

详细说明:
Python-iOS代码混淆-马甲包混淆工具（Python脚本混淆iOS工程）
https://zfj1128.blog.csdn.net/article/details/95482006

下载地址：
https://gitee.com/zfj1128/ZFJObsLib_dmg

备注：
如果可以请去给我的博客和git点个赞👍👍👍！